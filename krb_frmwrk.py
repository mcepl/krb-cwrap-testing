#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Library for testing Kerberos using applications in the virtual network
created with `cwrap <https://lwn.net/Articles/594863/>`__.

The best way to describe the use of the library is with the code
example::

    import os
    import subprocess
    import krb_frmwrk

    context.krb_stp = krb_frmwrk.KerberosSetup()

    new_val = ("LD_PRELOAD=libsocket_wrapper.so " +
               "SOCKET_WRAPPER_DIR={SOCKET_WRAPPER_DIR} " +
               "G_MESSAGES_DEBUG=all " +
               "G_DEBUG=fatal-criticals " +
               "SOCKET_WRAPPER_DEFAULT_IFACE={SOCKET_WRAPPER_DEFAULT_IFACE}"
              ).format(**context.krb_stp.test_env)
    new_env = os.environ().copy()
    new_env.update(dict((x.split('=') for x in new_val.split(' '))))

    pid = subprocess.Popen([cmd_name, 'paramter'],
                           env=new_env, stdout=PIPE, stderr=PIPE)
    out, err = pid.communicate()
    print('Running {}. (returncode {:d}), stdout:\n{}\nstderr:\n{}\n{}'.format(
          cmd_name, pid.returncode, out.strip(), err.strip(), '-' * 30))

Program ``cmd_name`` is now running with the false network information
pretending it runs on the network with all Kerberos servers running and
configured.

For this to work, the following packages (using their RHEL-7 names) are
required:

    * krb5-server
    + krb5-workstation
    + nss_wrapper
    + openssl
    + socket_wrapper

"""
# Copyright 2018, Matěj Cepl <mcepl@redhat.com>, Red Hat Corp.
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
# CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
from __future__ import print_function

import logging
import os
import os.path
import shutil
import signal
import subprocess
import tempfile
import threading
from string import Template


log = logging.getLogger('krb_frmwrk')

# Move to the configuration file
KRB_DOMAIN = 'mag.dev'
WRAP_HOSTNAME = "kdc.{}".format(KRB_DOMAIN)
WRAP_ALIASNAME = "alias.{}".format(KRB_DOMAIN)
WRAP_FAILNAME = "fail.{}".format(KRB_DOMAIN)
WRAP_IPADDR = '127.0.0.9'
WRAP_HTTP_PORT = '80'
WRAP_PROXY_PORT = '8080'
TIMEOUT = 30  # seconds?

TESTREALM = KRB_DOMAIN.upper()
KDC_DBNAME = 'db.file'
KDC_STASH = 'stash.file'
KDC_PASSWORD = 'modauthgssapi'

PKINIT_CA = 'cacert.pem'
PKINIT_KEY = 'key.pem'
PKINIT_USER_REQ = 'user.csr'
PKINIT_USER_CERT = 'user.pem'
PKINIT_KDC_REQ = 'kdccert.csr'
PKINIT_KDC_CERT = 'kdccert.pem'

USR_NAME = "maguser"
USR_PWD = "magpwd"
USR_NAME_2 = "maguser2"
USR_PWD_2 = "magpwd2"
USR_NAME_3 = "maguser3"
SVC_KTNAME = "httpd/http.keytab"
KEY_TYPE = "aes256-cts-hmac-sha1-96:normal"

KRB5_CONF_TEMPLATE = Template('''
[libdefaults]
  default_realm = $TESTREALM
  dns_lookup_realm = false
  dns_lookup_kdc = false
  rdns = false
  ticket_lifetime = 24h
  forwardable = yes
  default_ccache_name = FILE://$TESTDIR/ccaches/krb5_ccache_XXXXXX

[realms]
  $TESTREALM = {{
    kdc =$WRAP_HOSTNAME
    pkinit_anchors = FILE:$TESTDIR/$PKINIT_CA
  }}

[domain_realm]
  .{0} = $TESTREALM
  {0} = $TESTREALM

[dbmodules]
  $TESTREALM = {{
    database_name = $KDCDIR/$KDC_DBNAME
  }}
'''.format(KRB_DOMAIN))

KDC_CONF_TEMPLATE = Template('''
[kdcdefaults]
 kdc_ports = 88
 kdc_tcp_ports = 88
 restrict_anonymous_to_tgt = true
 pkinit_identity = FILE:$TESTDIR/$PKINIT_KDC_CERT,$TESTDIR/$PKINIT_KEY
 pkinit_anchors = FILE:$TESTDIR/$PKINIT_CA
 pkinit_indicator = na1
 pkinit_indicator = na2
 pkinit_indicator = na3

[realms]
 $TESTREALM = {
  master_key_type = aes256-cts
  max_life = 7d
  max_renewable_life = 14d
  acl_file = $KDCDIR/kadm5.acl
  dict_file = /usr/share/dict/words
  default_principal_flags = +preauth
  admin_keytab = $TESTREALM/kadm5.keytab
  key_stash_file = $KDCDIR/$KDC_STASH
 }
[logging]
  kdc = FILE:$KDCLOG
''')

OPENSSLCNF_TEMPLATE = Template('''
[req]
prompt = no
distinguished_name = $$ENV::O_SUBJECT

[ca]
CN = CA
C = US
OU = Insecure test CA do not use
O = $TESTREALM

[kdc]
C = US
O = $TESTREALM
CN = KDC

[user]
C = US
O = $TESTREALM
CN = maguser3

[exts_ca]
subjectKeyIdentifier = hash
authorityKeyIdentifier = keyid:always,issuer:always
keyUsage = nonRepudiation,digitalSignature,keyEncipherment,dataEncipherment,keyAgreement,keyCertSign,cRLSign
basicConstraints = critical,CA:TRUE

[components_kdc]
0.component=GeneralString:krbtgt
1.component=GeneralString:$TESTREALM

[princ_kdc]
nametype=EXPLICIT:0,INTEGER:1
components=EXPLICIT:1,SEQUENCE:components_kdc

[krb5princ_kdc]
realm=EXPLICIT:0,GeneralString:$TESTREALM
princ=EXPLICIT:1,SEQUENCE:princ_kdc

[exts_kdc]
subjectKeyIdentifier = hash
authorityKeyIdentifier = keyid:always,issuer:always
keyUsage = nonRepudiation,digitalSignature,keyEncipherment,keyAgreement
basicConstraints = critical,CA:FALSE
subjectAltName = otherName:1.3.6.1.5.2.2;SEQUENCE:krb5princ_kdc
extendedKeyUsage = 1.3.6.1.5.2.3.5

[components_client]
component=GeneralString:maguser3

[princ_client]
nametype=EXPLICIT:0,INTEGER:1
components=EXPLICIT:1,SEQUENCE:components_client

[krb5princ_client]
realm=EXPLICIT:0,GeneralString:$TESTREALM
princ=EXPLICIT:1,SEQUENCE:princ_client

[exts_client]
subjectKeyIdentifier = hash
authorityKeyIdentifier = keyid:always,issuer:always
keyUsage = nonRepudiation,digitalSignature,keyEncipherment,keyAgreement
basicConstraints = critical,CA:FALSE
subjectAltName = otherName:1.3.6.1.5.2.2;SEQUENCE:krb5princ_client
extendedKeyUsage = 1.3.6.1.5.2.3.4
''')


class TimeoutExpired(RuntimeError):
    pass


class Command(object):
    def __init__(self, cmd, shell=False, stdout=None, stderr=None, env=None,
                 preexec_fn=None):
        self.cmd = cmd
        self.shell = shell
        self.stdout = stdout
        self.stderr = stderr
        self.env = env
        self.preexec_fn = preexec_fn
        self.process = None
        log.debug(('cmd = %s, shell = %s, stdout = %s, stderr = %s, ' +
                   'preexec_fn = %s\nenv = %s'),
                  self.cmd, self.shell, self.stdout, self.stderr,
                  self.preexec_fn, self.env)

    def call(self, timeout):
        def target():
            self.process = subprocess.Popen(self.cmd, shell=self.shell,
                                            stdout=self.stdout,
                                            stderr=self.stderr,
                                            env=self.env,
                                            preexec_fn=self.preexec_fn)
            self.process.communicate()

        thread = threading.Thread(target=target)
        thread.start()

        thread.join(timeout)
        if thread.is_alive():
            self.process.terminate()
            thread.join()
            raise TimeoutExpired("Process {} timed out!".format(self.cmd))
        return self.process.returncode


class KerberosSetup(object):

    def __init__(self):
        test_dir = tempfile.mkdtemp(prefix='scratchdir')
        if os.path.exists(test_dir):
            shutil.rmtree(test_dir)
        os.makedirs(test_dir)
        self.test_log = os.path.join(test_dir, 'tests.log')

        self.processes = dict()
        self.test_env = dict()
        self.new_env = dict()

        self.setup_krb_infra(test_dir)

    @staticmethod
    def setup_wrappers(base):

        if subprocess.call(['pkg-config', '--exists', 'socket_wrapper']) != 0:
            raise ValueError('Socket Wrappers not available')

        if subprocess.call(['pkg-config', '--exists', 'nss_wrapper']) != 0:
            raise ValueError('NSS Wrappers not available')

        wrapdir = os.path.join(base, 'wrapdir')
        if not os.path.exists(wrapdir):
            os.makedirs(wrapdir)

        hosts_file = os.path.join(wrapdir, 'hosts')
        with open(hosts_file, 'w+') as f:
            print('{} {}'.format(WRAP_IPADDR, WRAP_HOSTNAME), file=f)
            print('{} {}'.format(WRAP_IPADDR, WRAP_ALIASNAME), file=f)
            print('{} {}'.format(WRAP_IPADDR, WRAP_FAILNAME), file=f)

        wenv = {'LD_PRELOAD': 'libsocket_wrapper.so libnss_wrapper.so',
                'SOCKET_WRAPPER_DIR': wrapdir,
                'SOCKET_WRAPPER_DEFAULT_IFACE': '9',
                'SOCKET_WRAPPER_PCAP_FILE': os.path.join(base, 'wrapped.pcap'),
                'WRAP_PROXY_PORT': WRAP_PROXY_PORT,
                'NSS_WRAPPER_HOSTNAME': WRAP_HOSTNAME,
                'NSS_WRAPPER_HOSTS': hosts_file}

        return wenv

    @staticmethod
    def setup_test_certs(testdir, testenv, testlog):

        opensslcnf = os.path.join(testdir, 'openssl.cnf')
        pkinit_key = os.path.join(testdir, PKINIT_KEY)
        pkinit_ca = os.path.join(testdir, PKINIT_CA)
        pkinit_kdc_req = os.path.join(testdir, PKINIT_KDC_REQ)
        pkinit_user_req = os.path.join(testdir, PKINIT_USER_REQ)
        pkinit_kdc_cert = os.path.join(testdir, PKINIT_KDC_CERT)
        pkinit_user_cert = os.path.join(testdir, PKINIT_USER_CERT)

        text = OPENSSLCNF_TEMPLATE.substitute(TESTREALM=TESTREALM)
        with open(opensslcnf, 'w+') as f:
            f.write(text)

        with (open(testlog, 'a')) as logfile:
            print(pkinit_key)
            if subprocess.call(["openssl", "genrsa", "-out", pkinit_key,
                                "2048"], stdout=logfile,
                               stderr=logfile, env=testenv,
                               preexec_fn=os.setsid) != 0:
                raise ValueError('Generating CA RSA key failed')

            testenv.update({'O_SUBJECT': 'ca'})
            if subprocess.call(["openssl", "req", "-config", opensslcnf,
                                "-new", "-x509", "-extensions", "exts_ca",
                                "-set_serial", "1", "-days", "100",
                                "-key", pkinit_key, "-out", pkinit_ca],
                               stdout=logfile, stderr=logfile, env=testenv,
                               preexec_fn=os.setsid) != 0:
                raise ValueError('Generating CA certificate failed')

            testenv.update({'O_SUBJECT': 'kdc'})
            if subprocess.call(["openssl", "req", "-config", opensslcnf,
                                "-new", "-subj", "/CN=kdc",
                                "-key", pkinit_key, "-out", pkinit_kdc_req],
                               stdout=logfile, stderr=logfile, env=testenv,
                               preexec_fn=os.setsid) != 0:
                raise ValueError('Generating KDC req failed')

            if subprocess.call(["openssl", "x509", "-extfile", opensslcnf,
                                "-extensions", "exts_kdc", "-set_serial", "2",
                                "-days", "100", "-req", "-CA", pkinit_ca,
                                "-CAkey", pkinit_key, "-out", pkinit_kdc_cert,
                                "-in", pkinit_kdc_req],
                               stdout=logfile, stderr=logfile, env=testenv,
                               preexec_fn=os.setsid) != 0:
                raise ValueError('Generating KDC certificate failed')

            testenv.update({'O_SUBJECT': 'user'})
            if subprocess.call(["openssl", "req", "-config", opensslcnf,
                                "-new", "-subj", "/CN=user",
                                "-key", pkinit_key, "-out", pkinit_user_req],
                               stdout=logfile, stderr=logfile, env=testenv,
                               preexec_fn=os.setsid) != 0:
                raise ValueError('Generating client req failed')

            if subprocess.call(["openssl", "x509", "-extfile", opensslcnf,
                                "-extensions", "exts_client",
                                "-set_serial", "3",
                                "-days", "100", "-req", "-CA", pkinit_ca,
                                "-CAkey", pkinit_key, "-out", pkinit_user_cert,
                                "-in", pkinit_user_req],
                               stdout=logfile, stderr=logfile, env=testenv,
                               preexec_fn=os.setsid) != 0:
                raise ValueError('Generating client certificate failed')

    def setup_kdc(self, testdir, wrapenv):

        # setup kerberos environment
        testlog = os.path.join(testdir, 'kerb.log')
        krb5conf = os.path.join(testdir, 'krb5.conf')
        kdcconf = os.path.join(testdir, 'kdc.conf')
        kdcdir = os.path.join(testdir, 'kdc')
        if os.path.exists(kdcdir):
            shutil.rmtree(kdcdir)
        os.makedirs(kdcdir)

        text = KRB5_CONF_TEMPLATE.substitute({'TESTREALM': TESTREALM,
                                              'TESTDIR': testdir,
                                              'KDCDIR': kdcdir,
                                              'KDC_DBNAME': KDC_DBNAME,
                                              'WRAP_HOSTNAME': WRAP_HOSTNAME,
                                              'PKINIT_CA': PKINIT_CA})
        with open(krb5conf, 'w+') as f:
            f.write(text)

        text = KDC_CONF_TEMPLATE.substitute({'TESTREALM': TESTREALM,
                                             'TESTDIR': testdir,
                                             'KDCDIR': kdcdir,
                                             'KDCLOG': testlog,
                                             'KDC_STASH': KDC_STASH,
                                             'PKINIT_CA': PKINIT_CA,
                                             'PKINIT_KDC_CERT': PKINIT_KDC_CERT,
                                             'PKINIT_KEY': PKINIT_KEY})
        with open(kdcconf, 'w+') as f:
            f.write(text)

        kdcenv = {'PATH': '/sbin:/bin:/usr/sbin:/usr/bin',
                  'KRB5_CONFIG': krb5conf,
                  'KRB5_KDC_PROFILE': kdcconf,
                  'KRB5_TRACE': os.path.join(testdir, 'krbtrace.log')}
        kdcenv.update(wrapenv)

        with (open(testlog, 'a')) as logfile:
            if subprocess.call(["kdb5_util", "create", "-W", "-s",
                                "-r", TESTREALM, "-P", KDC_PASSWORD],
                               stdout=logfile, stderr=logfile,
                               env=kdcenv, preexec_fn=os.setsid) != 0:
                raise ValueError('KDC Setup failed')

        self.setup_test_certs(testdir, kdcenv, testlog)

        with (open(testlog, 'a')) as logfile:
            kdcproc = subprocess.Popen(['krb5kdc', '-n'],
                                       stdout=logfile, stderr=logfile,
                                       env=kdcenv, preexec_fn=os.setsid)

        return kdcproc, kdcenv

    @staticmethod
    def kadmin_local(cmd, env, logfile):
        if subprocess.call(["kadmin.local", "-q", cmd],
                           stdout=logfile, stderr=logfile,
                           env=env, preexec_fn=os.setsid) != 0:
            raise ValueError('Kadmin local [{}] failed'.format(cmd))

    def setup_keys(self, testdir, env):
        testlog = os.path.join(testdir, 'kerb.log')

        svc_name = "HTTP/{}".format(WRAP_HOSTNAME)
        svc_keytab = os.path.join(testdir, SVC_KTNAME)
        cmd = "addprinc -randkey -e {} {}".format(KEY_TYPE, svc_name)
        with (open(testlog, 'a')) as logfile:
            self.kadmin_local(cmd, env, logfile)
        cmd = "ktadd -k {} -e {} {}".format(svc_keytab, KEY_TYPE, svc_name)
        with (open(testlog, 'a')) as logfile:
            self.kadmin_local(cmd, env, logfile)

        cmd = "addprinc -pw {} -e {} {}".format(USR_PWD, KEY_TYPE, USR_NAME)
        with (open(testlog, 'a')) as logfile:
            self.kadmin_local(cmd, env, logfile)

        cmd = "addprinc -pw {} -e {} {}".format(USR_PWD_2, KEY_TYPE,
                                                USR_NAME_2)
        with (open(testlog, 'a')) as logfile:
            self.kadmin_local(cmd, env, logfile)

        # alias for multinamed hosts testing
        alias_name = "HTTP/{}".format(WRAP_ALIASNAME)
        cmd = "addprinc -randkey -e {} {}".format(KEY_TYPE, alias_name)
        with (open(testlog, 'a')) as logfile:
            self.kadmin_local(cmd, env, logfile)
        cmd = "ktadd -k {} -e {} {}".format(svc_keytab, KEY_TYPE, alias_name)
        with (open(testlog, 'a')) as logfile:
            self.kadmin_local(cmd, env, logfile)

        cmd = "addprinc -nokey -e {} {}".format(KEY_TYPE, USR_NAME_3)
        with (open(testlog, 'a')) as logfile:
            self.kadmin_local(cmd, env, logfile)

        keys_env = {"KRB5_KTNAME": svc_keytab}
        keys_env.update(env)

        return keys_env

    @staticmethod
    def kinit_user(testdir, kdcenv):
        testlog = os.path.join(testdir, 'kinit.log')
        ccache = os.path.join(testdir, 'k5ccache')
        testenv = {'KRB5CCNAME': ccache}
        testenv.update(kdcenv)

        with (open(testlog, 'a')) as logfile:
            kinit = subprocess.Popen(["kinit", USR_NAME],
                                     stdin=subprocess.PIPE,
                                     stdout=logfile, stderr=logfile,
                                     env=testenv, preexec_fn=os.setsid)
            kinit.communicate('{}\n'.format(USR_PWD))
            if kinit.returncode != 0:
                raise ValueError('kinit failed')
        return testenv

    @staticmethod
    def kinit_certuser(testdir, kdcenv):
        testlog = os.path.join(testdir, 'kinit.log')
        ccache = os.path.join(testdir, 'k5ccache2')
        pkinit_user_cert = os.path.join(testdir, PKINIT_USER_CERT)
        pkinit_key = os.path.join(testdir, PKINIT_KEY)
        ident = "X509_user_identity=FILE:" + pkinit_user_cert \
            + "," + pkinit_key
        testenv = {'KRB5CCNAME': ccache}
        testenv.update(kdcenv)
        with (open(testlog, 'a')) as logfile:
            print('PKINIT for maguser3', file=logfile)
            if subprocess.call(["kinit", USR_NAME_3, "-X", ident],
                               stdin=subprocess.PIPE,
                               stdout=logfile, stderr=logfile,
                               env=testenv, preexec_fn=os.setsid) != 0:
                raise ValueError('kinit failed')
        return testenv

    @staticmethod
    def test_kinit(testdir, testenv, testlog):
        failed = False
        hdir = os.path.join(testdir, 'test_kinit')
        if os.path.exists(hdir):
            shutil.rmtree(hdir)
        os.mkdir(hdir)
        s_dir = os.path.realpath(os.path.dirname(__file__))

        log.debug('testlog = %s', testlog)
        with open(testlog, 'a') as logfile:
            try:
                req = Command([os.path.join(s_dir, 't_kinit')],
                              stdout=logfile, stderr=logfile, env=testenv,
                              preexec_fn=os.setsid).call(timeout=TIMEOUT)
                log.debug('req = %d', req)
            except TimeoutExpired:
                log.warning('Command t_kinit timed out.')
                failed = True
            except:
                raise
            else:
                failed = req == 0

        return failed

    def setup_krb_infra(self, tdir):
        """Setting up Kerberos infrastructure.

           Should be run in try: section before each test, because some
           methods could fail.
        """
        wrap_env = self.setup_wrappers(tdir)

        kdc_proc, kdc_env = self.setup_kdc(tdir, wrap_env)
        self.processes['KDC({:d})'.format(kdc_proc.pid)] = kdc_proc

        self.setup_keys(tdir, kdc_env)
        self.test_env = self.kinit_user(tdir, kdc_env)

        # support virtualenv
        self.test_env['PATH'] = os.environ.get('PATH', '')
        self.test_env['ViRTUAL_ENV'] = os.environ.get('VIRTUAL_ENV', '')

        self.test_env['DELEGCCACHE'] = os.path.join(tdir, 'httpd',
                                                    USR_NAME + '@' + TESTREALM)
        # self.test_kinit(tdir, self.test_env, self.test_log)

        self.test_env['MAG_GSS_NAME'] = USR_NAME + '@' + TESTREALM
        # self.test_kinit(tdir, self.test_env, self.test_log)

    def tear_down_krb_infra(self, tlog_name=None):
        """Tearing down Kerberos infrastructure.

           Should be run in finally: section after each test.
        """
        if tlog_name is None:
            tlog_name = self.test_log

        with (open(tlog_name, 'a')) as log_file:
            for p_name in self.processes:
                print("Killing {}".format(p_name), file=log_file)
                os.killpg(self.processes[p_name].pid, signal.SIGTERM)
