Kerberos testing framework
==========================

Python library for running programs inside of the special 
environment pretending to be Kerberos servers running on the 
network, in fact all these programs are running on the local 
computer in the fake cwrap-based environment.

Documentation is in the docstring of the program itself.
